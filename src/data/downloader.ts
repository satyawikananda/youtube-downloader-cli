import YoutubeMp3Downloader from "youtube-mp3-downloader"
import path from "path"
import url from "url"
import {isURL} from "../helpers/helper"

const downloader: any = new YoutubeMp3Downloader({
    ffmpegPath: "/c/ffmpeg/bin",
    outputPath: path.resolve(__dirname, "download"),
    youtubeVideoQuality: "highest",
    queueParallelism: 3,
    progressTimeout: 5000
})

export const download = (vidId: string, filename:string) => {
    return new Promise((resolve: any, reject:any) => {
        let videoId:any = vidId

        if(isURL(vidId)){
            const urlQuery = url.parse(vidId, true).query
            videoId = urlQuery.v
        }

        if(!videoId){
            throw new Error("Missing video id!")
        }

        downloader.download(videoId, filename)

        downloader.on("finished", (err: string, data:any) => {
            resolve(data)
        })

        downloader.on("error", (err:string) => {
            reject(err)
        })
    })
}

export default downloader