import commander from "commander"
import downloader from "./data/downloader"
import ora from "ora"

const spinner = ora("Downloading file...").start()
spinner.color = "red"
spinner.text = "Downloading..."

commander.version("0.1.0")
 .description("Simple youtube downloader cli")

commander.command("ytd")
 .requiredOption("'-l, --link <link>', 'A youtube video link or id'")
 .option('-n, --name [name]', 'Name of the downloaded file')
 .action((cmObj: any):void => {
    const {link, name} = cmObj
    downloader.download(link, name)
     .then((finishObj:any) => {
        spinner.succeed(`Finished downloading ${finishObj.videoTitle} in ${finishObj.stats.runtime} seconds`)
     })
     .catch((err:string) => {
        spinner.fail("Could not download this video")
        console.log(err)
     })

     downloader.downloader.on('progress', (progressObj: any) => {
        spinner.text = `${Number(progressObj.progress.percentage).toFixed(2)}% done`;
     })
 })

 commander.parse(process.argv)